Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/external/jquery/jquery-ui-datepicker.js');
Qunit.require('js/atlassian/atlassian.js');
Qunit.require('js/atlassian/inline-dialog.js');
Qunit.require('js/atlassian/aui-date-picker.js');

module('AUI Experimental Date Picker Unit Tests');

test('Date Picker API', function () {
    ok(typeof AJS.DatePicker === 'function', 'AJS.DatePicker exists');
    ok(typeof AJS.DatePicker.prototype.browserSupportsDateField === 'boolean', 'AJS.DatePicker.prototype.browserSupportsDateField exists');
    ok(typeof AJS.DatePicker.prototype.defaultOptions === 'object', 'AJS.DatePicker.prototype.defaultOptions exists');
    ok(typeof AJS.DatePicker.prototype.defaultOptions.overrideBrowserDefault === 'boolean', 'AJS.DatePicker.prototype.defaultOptions.overrideBrowserDefault exists');
    ok(typeof AJS.DatePicker.prototype.defaultOptions.firstDay === 'number', 'AJS.DatePicker.prototype.defaultOptions.firstDay exists');
    ok(typeof AJS.DatePicker.prototype.defaultOptions.languageCode === 'string', 'AJS.DatePicker.prototype.defaultOptions.languageCode exists');
    ok(typeof AJS.DatePicker.prototype.localisations === 'object', 'AJS.DatePicker.prototype.localisations exists');
});

test('Date Picker instance API (without polyfill)', function () {

    var datePicker, input;

    AJS.DatePicker.prototype.browserSupportsDateField = true;

    input = AJS.$('#test-input');
    datePicker = input.datePicker();

    ok(typeof datePicker === 'object', 'Date Picker instance object returned');
    ok(typeof datePicker.getField === 'function', 'Date Picker instance skeleton method getField exists');
    ok(typeof datePicker.getOptions === 'function', 'Date Picker instance skeleton method getOptions exists');
    ok(typeof datePicker.reset === 'function', 'Date Picker instance skeleton method reset exists');
    ok(typeof datePicker.hide === 'undefined', 'Date Picker instance polyfill-only method hide does not exist');
    ok(typeof datePicker.show === 'undefined', 'Date Picker instance polyfill-only method show does not exist');
    ok(typeof datePicker.destroyPolyfill === 'undefined', 'Date Picker instance polyfill-only method destroyPolyfill does not exist');

});

test('Date Picker instance API (with polyfill)', function () {

    var datePicker, input;

    AJS.DatePicker.prototype.browserSupportsDateField = false;

    input = AJS.$('#test-input');
    datePicker = input.datePicker();

    ok(typeof datePicker === 'object', 'Date Picker instance object returned');
    ok(typeof datePicker.getField === 'function', 'Date Picker instance skeleton method getField exists');
    ok(typeof datePicker.getOptions === 'function', 'Date Picker instance skeleton method getOptions exists');
    ok(typeof datePicker.reset === 'function', 'Date Picker instance skeleton method reset exists');
    ok(typeof datePicker.hide === 'function', 'Date Picker instance polyfill-only method hide exists');
    ok(typeof datePicker.show === 'function', 'Date Picker instance polyfill-only method show exists');
    ok(typeof datePicker.destroyPolyfill === 'function', 'Date Picker instance polyfill-only method destroyPolyfill exists');

});