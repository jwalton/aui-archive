define("MainCtrl", ["MainModule", 'Editors'], function(sandboxModule){
    
    sandboxModule.controller("MainCtrl", MainCtrl);

    function MainCtrl($scope, $http, saveService) {
        Editors.commands.save.exec = function() {
            $scope.save();
            $scope.$apply();
        };
        $scope.isFlatpack = (SANDBOX.env === "flatpack");
        $scope.editors = aceEditors;
        $scope.currentSnippetId = null;
        $scope.currentComponent = null;
        $scope.publishURL = "";
        $scope.canSave = false;
        $scope.AUIVersion = AJS.version

        $scope.deleteSnippetDialog = new AJS.Dialog({width:400, height:200, id:"delete-snippet-dialog"});
        $scope.deleteSnippetDialog.addHeader("Delete Snippet");
        $scope.deleteSnippetDialog.addPanel("Panel 1", "", "panel-body");
        $scope.deleteSnippetDialog.addButton("Delete",  null);
        $scope.deleteSnippetDialog.addLink("Cancel",  function(dialog){
            dialog.hide();
        }, "");

        $scope.keyboardShortcutsDialog = new AJS.Dialog({width:600, height:400, id:"keyboard-shortcuts-dialog"});
        $scope.keyboardShortcutsDialog.addHeader("Sandbox Keyboard Shortcuts");
        $scope.keyboardShortcutsDialog.addPanel("Panel 1", AJS.$("#keyboard-shortcuts-dialog-content").html(), "panel-body");
        $scope.keyboardShortcutsDialog.addLink("Cancel",  function(dialog){
            dialog.hide();
        }, "");

        $scope.aboutDialog = new AJS.Dialog({width:600, height:400, id:"keyboard-shortcuts-dialog"});
        $scope.aboutDialog.addHeader("About Sandbox");
        $scope.aboutDialog.addPanel("Panel 1", AJS.$("#about-dialog-content").html(), "panel-body");
        $scope.aboutDialog.addLink("Close",  function(dialog){
            dialog.hide();
        }, "");

        $scope.updateSnippetId = function(newId){
            $scope.currentSnippetId = newId;
        }

        $scope.updateCurrentComponent = function(newId){
            $scope.currentComponent= newId;
        }

        $scope.openKeyboardShortcuts = function(){
            $scope.keyboardShortcutsDialog.show();
        }

        $scope.openAbout = function(){
            $scope.aboutDialog.show();
        }
        
        $scope.reset = function() {
            //Can remove them here
            // $scope.currentSnippetId = null;
            $scope.currentComponent = null;
            _.each($scope.editors, function(editor, key) {
                editor.setValue('');
            });
        };
        $scope.save = function() {
            AJS.$("#save-wait-icon").css("display", "inline-block");

            saveService.newSnippetId();
            var snippet = saveService.saveCode(null, $scope.getCode());
            $scope.currentSnippetId = snippet.id;
            $scope.currentComponent = null;
            
            setTimeout(function(){
                AJS.$("#save-wait-icon").hide();
            }, 250);
        };
        
        $scope.openSnippet = function(snippetId) {
            var snippet = saveService.getSnippetById(snippetId);
            $scope.currentSnippetId = snippet.id;
            $scope.updateCurrentComponent();
            $scope.editors.html.setValue('');
            $scope.editors.html.insert(snippet.html);
            $scope.editors.js.setValue('');
            $scope.editors.js.insert(snippet.js);
            $scope.editors.css.setValue('');
            $scope.editors.css.insert(snippet.css);
        };

        $scope.hoverSnippetLink = function($event){
            AJS.$($event.target).find(".remove-snippet-icon").removeClass("hidden");
        }

        $scope.unhoverSnippetLink = function($event){
            if(!AJS.$($event.toElement).parent().is($event.target)){
                AJS.$($event.target).find(".remove-snippet-icon").addClass("hidden");   
            }
        }

        $scope.isActive = function(type, id) {
            if(type === "snippet"){
                return id === $scope.currentSnippetId ? "aui-nav-selected" : "";
            }

            if(type === "component"){
                return id === $scope.currentComponent ? "aui-nav-selected" : "";    
            }
        };

        $scope.getCode = function() {
            return {
                html: $scope.editors.html.getValue(),
                js: $scope.editors.js.getValue(),
                css: $scope.editors.css.getValue()
            };
        };

        // If id is null, binds to all editors
        $scope.bindToEditorChange = function(id, callback) {
            var ids = id ? [id] : _.keys($scope.editors);
            _.each(ids, function(id) {
                $scope.editors[id].getSession().on('change', callback);
            });
        };

        $scope.changePublishURL = function(newURL){
            $scope.publishURL = newURL;
        }

        $scope.setCode = function(html, js, css){
            if(js)
            $scope.editors.js.insert(js);

            if(css)
            $scope.editors.css.insert(css);

            if(html)
            $scope.editors.html.insert(html);       
        }

        $scope.runJavascript = function(){
            AJS.$('body').trigger('runJavascript');
        }

        $scope.editors.js.on("change", function(e){
            if($scope.editors.js.getValue() == ""){
                AJS.$("#run-js-button").attr("aria-disabled", "true");
            } else if(AJS.$("#run-js-button").attr("aria-disabled") == "true"){
                AJS.$("#run-js-button").attr("aria-disabled", "false");
            }
        });
    }
});