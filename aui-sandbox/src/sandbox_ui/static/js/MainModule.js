// this is loaded first so we'll set up the global namespace here
define("MainModule", ['libraries/angular-min'], function(angular){
    var sandboxModule = angular.module('sandbox', []);
    return sandboxModule;
});