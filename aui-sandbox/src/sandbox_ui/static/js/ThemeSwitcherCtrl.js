define("ThemeSwitcherCtrl", ["MainModule"], function(sandboxModule) {

    sandboxModule.controller("ThemeSwitcherCtrl", ThemeSwitcherCtrl);
    
    function ThemeSwitcherCtrl($scope) {
        $scope.themes = [
            {
                id: 'chrome',
                title: 'Chrome',
                displayed: true
            },
            {
                id: 'clouds',
                title: 'Clouds',
                displayed: true
            },
            {
                id: 'idle_fingers',
                title: 'Idle fingers',
                displayed: true
            },
            {
                id: 'solarized_light',
                title: 'Solarized light',
                displayed: true
            },
            {
                id: 'twilight',
                title: 'Twilight',
                displayed: true
            },
            {
                id: 'vibrant_ink',
                title: 'Vibrant ink',
                displayed: true
            }
        ];

        $scope.getThemeById = function(themeId) {
            return _.find($scope.themes, function(theme) {
                return theme.id === themeId;
            });
        };

        $scope.currentTheme = $scope.getThemeById("clouds");

        $scope.isCurrentTheme = function(theme){
            return (theme.title === $scope.currentTheme.title) ? "selected-theme" : "";
        };

        $scope.switchToTheme = function(theme) {
            var themePath = "ace/theme/" + theme.id;

            $scope.editors.html.setTheme(themePath);
            $scope.editors.js.setTheme(themePath);
            $scope.editors.css.setTheme(themePath);

            //In IE the first editor in the chain does not set properly, set it again at the end
            $scope.editors.html.setTheme(themePath);
            $scope.currentTheme = theme;
            if(localStorage){
                localStorage.setItem("theme", JSON.stringify(theme));
            }
        }
        if(localStorage){
            var lstheme = localStorage.getItem("theme");
            if(lstheme === undefined || lstheme === null) {
                var store = JSON.stringify({
                    id: "clouds",
                    title: "Clouds"
                });
                localStorage.setItem("theme", store);
            } else {
                var restore = JSON.parse(lstheme);
                $scope.switchToTheme(restore);
            }
        }
        window.setTimeout(function(){ 
            AJS.$(".selected-theme").attr("aria-checked", "true").addClass("checked");
        }, 0);
    }
});