define("ComponentPanelCtrl", ['MainModule'], function(sandboxModule) {

    sandboxModule.controller("ComponentPanelCtrl", ComponentPanelCtrl);

    function ComponentPanelCtrl($scope) {

        //Populate the library with all available components
        $scope.components = [];
        $scope.patterns = [];
        for(i in SANDBOX.Library.components){
            $scope.components.push({
                id: i,
                title: i.charAt(0).toUpperCase() + i.slice(1),
                display: true
            });
        }

            for(i in SANDBOX.Library.patterns){
            $scope.patterns.push({
                id: i,
                title: i.charAt(0).toUpperCase() + i.slice(1),
                display: true
            });
        }

        $scope.components.sort(alphabetically);
        $scope.patterns.sort(alphabetically);

        function alphabetically(a,b){
            if(a.title < b.title){
                return -1;
            }
            if(a.title > b.title){
                return 1;
            }
            return 0;
        }

        $scope.addComponentToEditor = function(componentId) {
            $scope.updateSnippetId(null);
            $scope.updateCurrentComponent(componentId);
            
            var data = SANDBOX.Library.find(componentId);

            var jsEditor = $scope.editors.js;
            var htmlEditor = $scope.editors.html;

            var jsLastRow = jsEditor.getLastVisibleRow() + 1;
            var htmlLastRow = htmlEditor.getLastVisibleRow() + 1;

            jsEditor.gotoLine(jsLastRow, 0, true);
            htmlEditor.gotoLine(htmlLastRow, 0, true);

            //clear editors first
            jsEditor.selectAll();
            htmlEditor.selectAll();

            jsEditor.insert(data.js);
            htmlEditor.insert(data.html);

            //run the javascript as well
            var js = $scope.editors.js.getValue(),
                iframeWindow =  AJS.$('#output-frame')[0].contentWindow;
            iframeWindow.AJS.$('<script>').html(js).appendTo(AJS.$("body", iframeWindow.contentWindow));
        };
    }
});

