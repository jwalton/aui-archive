//Require JS
requirejs.config({
    shim: {
        'libraries/angular-min': {
            exports: 'angular'
        }
    },
    baseUrl: 'static/js',
    paths: {
        aui: '../aui/js',
        libraries:'libraries',
        templates: '../template'
    }
});
var dependencies = [
            'libraries/underscore',
            'MainModule',
            'SaveService',
            'Template',
            "templates/registerall",
            'Editors',
            'MainCtrl',
            'OutputCtrl',
            'TogglePanelCtrl',
            'ComponentPanelCtrl',
            'WorkspacePanelCtrl',
            'SaveCtrl',
            'ThemeSwitcherCtrl',
            'DeleteSnippetCtrl'];

requirejs(dependencies, function(){
    AJS.$(function($){

        //We need to start the app manually because we're using requirejs.
        angular.bootstrap(document,["sandbox"]);
        // Sidebar stuff
        // var test = requirejs('aui/aui-all');
        var MIN_WIDTH = 200,
            $body = $("body"),
            $navBar = $("#library-nav"),
            $rightPanels = $("#sandbox-content-area"),
            $panelSplitters = $rightPanels.find(".splitter-handle");

        function setPanesWidth($leftPanel, $rightPanel, $splitter, mouseX) {
            var currentPosition = $splitter.offset().left;
            var diff = mouseX - currentPosition;
            var $stretchPanels = $(".stretch-panels");
            var percentageDiff = diff/$stretchPanels.width() * 100;
            var currentLeftPercentage = ($leftPanel.width()/$stretchPanels.width()) * 100;
            var currentRightPercentage = ($rightPanel.width()/$stretchPanels.width()) * 100;
           
           //set widths
           if(($leftPanel.width() >= MIN_WIDTH && diff < 0) || ($rightPanel.width() >= MIN_WIDTH && diff > 0)){
               $leftPanel.css("width", (currentLeftPercentage + percentageDiff) + "%");
               $rightPanel.css("width", (currentRightPercentage - percentageDiff) + "%");
           }

            _.each(aceEditors, function(editor) {
                editor.resize();
            });
        }

        $panelSplitters.on("mousedown.stretch", function(evt){
            var $currentSplitter = $(this);
            var $currentPanel = $currentSplitter.closest("section.stretch"); 
            var $nextPanel = $($currentPanel.nextAll().filter("section.stretch:visible")[0]);
            var $splitter = $(evt.target);
            
            var mouseupHandler = function () {
                $body.off('mousemove.stretch');
                $("#output-frame").css("pointer-events", "auto");
            };
     
            $body.on('mousemove.stretch',function (evt) {
                evt.preventDefault();
                $("#output-frame").css("pointer-events", "none");
                setPanesWidth($currentPanel, $nextPanel, $splitter, evt.pageX);
            });

            $body.one('mouseup mouseleave', mouseupHandler);
        });

        //show warning message if no local storage
        if(!window.localStorage){
            AJS.messages.warning("#header", {
                id: 'no-local-storage-message',
                title: "Local Version",
                body: "This browser does not support local storage, you will not be able to save your code snippets",
                insert: "prepend"               
            });  
            $("#snippet-buttons").hide();  
        }
        // this loads after everything else, remove the loading screen here
        window.setTimeout(function(){
            var $loadingScreen = $(".loading-screen");
            $loadingScreen.addClass("hidden");
            $loadingScreen.on("transitionend", function(){
                $loadingScreen.remove();
            });   
        }, 500); //wait 1 second so it doesn't flash if it's too quick
    });
});
