define("OutputCtrl", ["MainModule"], function(sandboxModule) {
	
    sandboxModule.controller("OutputCtrl", OutputCtrl);

	function OutputCtrl($scope) {
		var iframeWindow, iframeDocument;
		var $head, $body, $css, $js;

		AJS.$('body').bind('runJavascript', function() {
			$scope.outputAndRunJs();
		});

		$scope.bindToEditorChange(null, function() {
			$scope.output();
		});

		function setupIframe(){
			// set up the guts of the iframe
			iframeWindow = AJS.$('#output-frame')[0].contentWindow;
			iframeDocument = iframeWindow.document;
			iframeDocument.write("<!DOCTYPE html>");
			iframeDocument.write("<html>");
			iframeDocument.write("<head>");
			// Inject AUI CSS
            if(SANDBOX.env === "flatpack") {
                iframeDocument.write('<link rel="stylesheet" type="text/css" href="../aui/css/aui-all.css">');
            } else {
                iframeDocument.write('<link rel="stylesheet" type="text/css" href="static/aui/css/aui-all.css">');
            }			
            iframeDocument.write('<link rel="stylesheet" type="text/css" href="static/css/component-output.css">');
			iframeDocument.write("</head>");
			iframeDocument.write("<body></body>");
			iframeDocument.write("</html>");
			iframeDocument.close();

			// Can't use jQuery here as it will try to load the script with XmlHttpRequest
			var script = iframeDocument.createElement('script');
			script.type = 'text/javascript';
			if(SANDBOX.env === "flatpack") {
				script.src = '../aui/js/aui-all.js';
			} else {
				script.src = 'static/aui/js/aui-all.js';
			}
			AJS.$("head", iframeDocument)[0].appendChild(script);
		}

		$scope.reset = function() {
			// Containers to dump code into
			$head = AJS.$("head", iframeDocument);
			$body = AJS.$("body", iframeDocument).addClass("aui-theme-default").addClass("aui-layout");
			$body.css("background", "#fff");
			$css = AJS.$("<style>").appendTo($head);
			$js = null; // JS is special. Can't just dump into the same <script> tag and expect it to run.
		};


		$scope.output = function() {
			var html = $scope.editors.html.getValue();
			var css = $scope.editors.css.getValue();
			
			//change background colour of preview depending on what component it is
			var pageLevelComponents = ["pageHeader", "appheader", "horizontalNav"],
				isPageLevel = _.find(pageLevelComponents, function(component){
					return component == $scope.currentComponent
				});

			if(isPageLevel){
				$body.css("background", "#f5f5f5");
			} else {
				$body.css("background", "#fff");
			}

		    try {
		    	$body.html(html);
		    	$css.html(css);

		    	//hack for multi-step progress tracker.  Move this into a css file when time permits.
				$body.find(".inverted-progress-tracker-example").css({
					background: "#f5f5f5",
					padding: "10px"
				});
		    } catch(e) {
		    	// Ignore
		    }
		};

		$scope.outputAndRunJs = function() {
			$scope.reset();
			$scope.output();

			var js = $scope.editors.js.getValue();
			if ($js) $js.remove();
			$js = iframeWindow.AJS.$('<script>').html(js).appendTo($body);
		};

		//run initial setup
		setupIframe();
		$scope.reset();
	}
});