define("WorkspacePanelCtrl", ["MainModule"], function(sandboxModule) {

    sandboxModule.controller("WorkspacePanelCtrl", WorkspacePanelCtrl);

    function WorkspacePanelCtrl($scope, saveService) {
    	$scope.getSnippets = function() {
    		if(localStorage){
    			return saveService.getSnippets();
    		}
    	};
    }
});