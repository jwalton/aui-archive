define("DeleteSnippetCtrl", ['MainModule'], function(sandboxModule) {

    sandboxModule.controller("DeleteSnippetCtrl", DeleteSnippetCtrl);
    function DeleteSnippetCtrl($scope, saveService) {

        //set up delete snippet dialog
        $scope.checkDeleteSnippet = function(snippet, $event){
            // $scope.deleteSnippet(snippet);
            AJS.$($event.target).addClass("hidden");
            AJS.$($scope.deleteSnippetDialog.getPanel(0,0).body).html("Are you sure you want to delete " + snippet.title + "?")
            var $deleteButton = AJS.$($scope.deleteSnippetDialog.page[0].button[0].item);
            $deleteButton.unbind("click");
            $deleteButton.click(function(){
                $scope.deleteSnippet(snippet);
                $scope.deleteSnippetDialog.hide();
                AJS.$("#workspace-bar .li").click();
            });
            $scope.deleteSnippetDialog.show();

        }

        $scope.deleteSnippet = function(snippet){
            saveService.deleteSnippet(snippet.index);
        }
    }
});