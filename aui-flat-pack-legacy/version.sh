#!/bin/bash
# ${TARGETDIR} ${V} ${VERSIONREGEX} ${TIMESTAMP} ${TIMEREGEX}
target=$1
version=$2
versionRegex=$3
timestamp=$4
timestampRegex=$5

# don't hit all files or PNGs get corrupted under Cygwin.
# single action find+sed was breaking so leave the multi-line version in.
# do the same for both mac and pc for consistency.

environment=`uname -s`
if [[ "$environment" == *CYGWIN* ]]
then
    echo "Cygwin detected"
    echo "Setting Version ($version)"
    find $target -name "*.css" -type f -exec sed -i $versionRegex {} \;
    find $target -name "*.js" -type f -exec sed -i $versionRegex {} \;
    find $target -name "*.html" -type f -exec sed -i $versionRegex {} \;
    echo "Applying timestamp ($timestamp)"
    find $target -name "*.css" -type f -exec sed -i $timestampRegex {} \;
    find $target -name "*.js" -type f -exec sed -i $timestampRegex {} \;
    find $target -name "*.html" -type f -exec sed -i $timestampRegex {} \;
else
    # note the '' in this version of the sed command
    echo "Setting Version ($version)"
    find $target -name "*.css" -type f -exec sed -i '' $versionRegex {} \;    
    find $target -name "*.js" -type f -exec sed -i '' $versionRegex {} \;    
    find $target -name "*.html" -type f -exec sed -i '' $versionRegex {} \;    
    echo "Applying timestamp ($timestamp)"
    find $target -name "*.css" -type f -exec sed -i '' $timestampRegex {} \;    
    find $target -name "*.js" -type f -exec sed -i '' $timestampRegex {} \;    
    find $target -name "*.html" -type f -exec sed -i '' $timestampRegex {} \;
fi
